Reparsing block
----------
Full reparse
----------
After typing
----------
sub somesub {
    say 'hi';
    m/<caret>
    say 'hi';
}

----------
Psi structure
----------
Perl5
  PsiPerlSubDefinitionImpl(SUB_DEFINITION)@main::somesub
    PsiElement(Perl5: sub)('sub')
    PsiWhiteSpace(' ')
    PerlSubNameElementImpl(Perl5: subname)('somesub')
    PsiWhiteSpace(' ')
    PsiPerlBlockImpl(Perl5: BLOCK)
      PsiElement(Perl5: {)('{')
      PsiWhiteSpace('\n    ')
      PsiPerlStatementImpl(Perl5: STATEMENT)
        PsiPerlPrintExprImpl(Perl5: PRINT_EXPR)
          PsiElement(Perl5: say)('say')
          PsiWhiteSpace(' ')
          PsiPerlCallArgumentsImpl(Perl5: CALL_ARGUMENTS)
            PsiPerlStringSqImpl(Perl5: STRING_SQ)
              PsiElement(Perl5: QUOTE_SINGLE_OPEN)(''')
              PerlStringContentElementImpl(Perl5: STRING_CONTENT)('hi')
              PsiElement(Perl5: QUOTE_SINGLE_CLOSE)(''')
        PsiElement(Perl5: ;)(';')
      PsiWhiteSpace('\n    ')
      PsiErrorElement:Unexpected token
        PsiElement(Perl5: m)('m')
      PsiErrorElement:Unexpected token
        PsiElement(Perl5: r{)('/')
      PsiElement(Perl5: regex)('\n    say 'hi';\n}\n')
      PsiErrorElement:<func definition>, <method definition>, <statement>, <sub definition>, Perl5: #@abstract, Perl5: #@deprecated, Perl5: #@inject, Perl5: #@method, Perl5: #@noinspection, Perl5: #@override, Perl5: #@returns, Perl5: #@type, Perl5: #@unknown, Perl5: BLOCK_NAME, Perl5: POD, Perl5: TryCatch::, Perl5: __DATA__, Perl5: __END__, Perl5: case, Perl5: default, Perl5: for, Perl5: foreach, Perl5: format, Perl5: fp_after, Perl5: fp_around, Perl5: fp_augment, Perl5: fp_before, Perl5: given, Perl5: if, Perl5: nyi, Perl5: switch, Perl5: unless, Perl5: until, Perl5: when, Perl5: while or Perl5: { expected
        <empty list>
